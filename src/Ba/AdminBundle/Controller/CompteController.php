<?php

namespace Ba\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ba\BuzzAnnonceBundle\Entity\Compte\CompteManager;
use Ba\BuzzAnnonceBundle\Form\Type\CompteType;
use Ba\BuzzAnnonceBundle\Manager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CompteController extends Controller {

    /**
     * @Route("/", name="compte_list")
     * @Template()
     */
}