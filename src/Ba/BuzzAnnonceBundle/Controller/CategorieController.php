<?php

namespace Ba\BuzzAnnonceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ba\BuzzAnnonceBundle\Entity\Categorie;
use Ba\BuzzAnnonceBundle\Form\Type\CategorieType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategorieController extends Controller
{
    /**
     * @Route("/new", name="categorie_new")
     * @Template()
     */
    public function newAction()
    {
        $request = $this->get('request'); // On récupère l'objet request via le service container
        $categorie = new Categorie(); // On créé notre objet Categorie vierge

        $form = $this->get('form.factory')->create(new CategorieType(), $categorie); // On bind l'objet Categorie à notre formulaire CategorieType

        if ('POST' == $request->getMethod()) { // Si on a posté le formulaire
            $form->bindRequest($request); // On bind les données du form
            //if ($form->isValid()) { // Si le formulaire est valide
                $this->get('ba_buzz_annonce.categorie_manager')->save($categorie); // On utilise notre Manager pour gérer la sauvegarde de l'objet

                // On envoi une 'flash' pour indiquer à l'utilisateur que le bureau est ajouté
                $this->get('session')->setFlash('notice', 
                    $this->get('translator')->trans('Votre categorie a bien été posté.')
                );

                // On redirige vers la page de modification du bureau
               // return new RedirectResponse($this->generateUrl('categorie_edit', array(
                //    'categorieId' => $categorie->getCptId()
               // )));
            return new RedirectResponse($this->generateUrl('_Admin_categorie_list'));

            //}
        }

        return array('form' => $form->createView(), 'categorie' => $categorie); // On passe à Twig l'objet form et notre objet categorie
                
        //return array();
    }

    /**
     * @Route("/delete/{id}", name="categorie_delete"))
     * @Template()
     */
    public function deleteAction($id) {
        $this->get('ba_buzz_annonce.categorie_manager')->deleteById($id);
        $cptArr = $this->get('ba_buzz_annonce.categorie_manager')->findAll();
        
        return $this->render('BaBuzzAnnonceBundle:Categorie:list.html.twig', array('categories' => $cptArr));
    }
    
    /**
     * @Route(name="categorie_list")
     * @Template()
     */
    public function listAction() { 
        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user->getCptAdmin())
            return new RedirectResponse($this->generateUrl('home'));
        $cptArr = $this->get('ba_buzz_annonce.categorie_manager')->findAll();

        return array('categories' => $cptArr); // On passe à Twig la liste des categories
    }
}
