<?php

namespace Ba\BuzzAnnonceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ba\BuzzAnnonceBundle\Entity\Compte;
use Ba\BuzzAnnonceBundle\Form\Type\CompteType;
use Ba\BuzzAnnonceBundle\Form\Type\LoginType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\SecurityContext;

class CompteController extends Controller {

    /**
     * @Route("/new", name="compte_new")
     * @Template()
     */
    public function newAction() {
        $request = $this->get('request'); // On récupère l'objet request via le service container
        $compte = new Compte(); // On créé notre objet Compte vierge

        $form = $this->get('form.factory')->create(new CompteType(), $compte); // On bind l'objet Compte à notre formulaire CompteType

        if ('POST' == $request->getMethod()) { // Si on a posté le formulaire
            $form->bindRequest($request); // On bind les données du form
            //if ($form->isValid()) { // Si le formulaire est valide
            $compte->setCptAdmin(false);
            $compte->setCptActif(true);
            $this->get('ba_buzz_annonce.compte_manager')->save($compte); // On utilise notre Manager pour gérer la sauvegarde de l'objet
            // On envoi une 'flash' pour indiquer à l'utilisateur que le bureau est ajouté
            $this->get('session')->setFlash('notice', $this->get('translator')->trans('Votre compte a bien été créé. Vous pouvez dès à présent poster des annonces.')
            );

            // On redirige vers la page de modification du bureau
            //return new RedirectResponse($this->generateUrl('compte_edit', array(
            //                   'compteId' => $compte->getCptId()
            //              )));
            return new RedirectResponse($this->generateUrl('home'));
            //}
        }

        return array('form' => $form->createView(), 'compte' => $compte); // On passe à Twig l'objet form et notre objet compte
        //return array();
    }

    /**
     * @Route("/edit/{compteId}", name="compte_edit")
     * @Template()
     */
    public function editAction($compteId) {
        $user = $this->get('security.context')->getToken()->getUser();
        if (!$user->getCptAdmin())
            return new RedirectResponse($this->generateUrl('home'));
        $request = $this->get('request');

        if ('POST' == $request->getMethod()) { // Si on a posté le formulaire
            // On vérifie que l'ID du bureau existe
            $compte = $this->get('ba_buzz_annonce.compte_manager')->findById($compteId);

            if (!$compte/* = $this->get('ba_buzz_annonce.compte_manager')->getById($compteId) */) {
                throw new NotFoundHttpException(
                        $this->get('translator')->trans('This compte does not exist.')
                );
            }

            /* prefill mdp field, not working yet
              $factory = $this->get('security.encoder_factory');
              $encoder = $factory->getEncoder($compte);
              $password = $encoder->encodePassword($compte->getCptMdp(), $user->getSalt());
              $compte->setCptMdp($password);
             */
            // On bind le bureau récupéré depuis la BDD au formulaire pour modification
            $form = $this->get('form.factory')->create(new CompteType(), $compte);


            // Si l'utilisateur soumet le formulaire
            if ('POST' == $request->getMethod()) {
                $form->bindRequest($request);
                $this->get('ba_buzz_annonce.compte_manager')->update($compte);
            }
            $format = $this->getRequest()->getRequestFormat();

            //return array('form' => $form->createView(), 'compte' => $compte);
            return $this->render('BaBuzzAnnonceBundle:Compte:new.html.twig', array('form' => $form->createView(), 'compte' => $compte)); // On change le template par défaut et on réutilise celui de add qui est le même
        } else {
            $cpt = $this->get('ba_buzz_annonce.compte_manager')->getById($compteId);
            $form = $this->get('form.factory')->create(new CompteType(), $cpt);
            return $this->render('BaBuzzAnnonceBundle:Compte:new.html.twig', array('form' => $form->createView(), 'compte' => $cpt));
        }
    }

    /**
     * @Route("/unactivate/{id}", name="compte_unactivate"))
     * @Template()
     */
    public function unactivateAction($id) {
        $user = $this->get('security.context')->getToken()->getUser();
        if (!$user->getCptAdmin())
            return new RedirectResponse($this->generateUrl('home'));
        $cpt = $this->get('ba_buzz_annonce.compte_manager')->getById($id);
        $cpt->setCptActif(false);
        $this->get('ba_buzz_annonce.compte_manager')->update($cpt);
        $this->get('session')->setFlash('notice', $this->get('translator')->trans('Compte mise à jour'));
        return new RedirectResponse($this->generateUrl('home'));
    }

    /**
     * @Route("/activate/{id}", name="compte_activate"))
     * @Template()
     */
    public function activateAction($id) {
        $user = $this->get('security.context')->getToken()->getUser();
        if (!$user->getCptAdmin())
            return new RedirectResponse($this->generateUrl('home'));
        $cpt = $this->get('ba_buzz_annonce.compte_manager')->getById($id);
        $cpt->setCptActif(true);
        $this->get('ba_buzz_annonce.compte_manager')->update($cpt);
        $this->get('session')->setFlash('notice', $this->get('translator')->trans('Compte mise à jour'));
        return new RedirectResponse($this->generateUrl('home'));
    }

    /**
     * @Route("/delete/{id}", name="compte_delete"))
     * @Template()
     */
    public function deleteAction($id) {
        $user = $this->get('security.context')->getToken()->getUser();
        if (!$user->getCptAdmin())
            return new RedirectResponse($this->generateUrl('home'));
        $cpt = $this->get('ba_buzz_annonce.compte_manager')->getById($id);
        $cpt->setCptActif(false);
        $this->get('ba_buzz_annonce.compte_manager')->update($cpt);
        $this->get('session')->setFlash('notice', $this->get('translator')->trans('Compte mise à jour'));
        return new RedirectResponse($this->generateUrl('home'));
    }

    /**
     * Show the profile of the current user
     * @Route("/show", name="compte_show")
     * @Template()
     */
    public function showAction() {
        return array();
    }

    /**
     * @Route("/login", name="compte_login")
     * @Template()
     */
    public function loginAction() {
        $request = $this->get('request');
        $session = $request->getSession();

        // Permet la récupération des erreurs
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        $user = $this->get('security.context')->getToken()->getUser();
        $lastUserName = $session->get(SecurityContext::LAST_USERNAME);
        $form = $this->get('form.factory')->create(new LoginType(), new Compte());

        return $this->render('BaBuzzAnnonceBundle:Compte:login.html.twig', array('form' => $form->createView(), 'compte' => new Compte(), 'error' => $error));
    }

    /**
     * @Route("/logout", name="compte_logout")
     * @Template()
     */
    public function logoutAction() {
        return new RedirectResponse($this->generateUrl('home'));
    }

    /**
     * @Route(name="compte_list")
     * @Template()
     */
    public function listAction() {
        $user = $this->get('security.context')->getToken()->getUser();
        if (!$user->getCptAdmin())
            return new RedirectResponse($this->generateUrl('home'));
        $cptArr = $this->get('ba_buzz_annonce.compte_manager')->findAll();

        return array('comptes' => $cptArr, 'user' => $user); // On passe à Twig la liste des comptes
    }

}
