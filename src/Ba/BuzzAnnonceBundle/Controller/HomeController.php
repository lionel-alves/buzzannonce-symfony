<?php

namespace Ba\BuzzAnnonceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ba\BuzzAnnonceBundle\Entity\Categorie;
use Ba\BuzzAnnonceBundle\Entity\Annonce;
use Ba\BuzzAnnonceBundle\Form\Type\CategorieType;

class HomeController extends Controller {

    /**
     * @Route("/", name="home")
     * @Template()
     */
    public function indexAction() {
        $form = $this->get('form.factory')->create(new CategorieType(), new Categorie());
        $lCategorie = $this->get('ba_buzz_annonce.categorie_manager')->findAll();
        
        $index = 0;
        $request = $this->get('request');
        if ('POST' == $request->getMethod()) {
            if ($request->request->get('indexToDisplay')) {
                $index = intval($request->request->get('indexToDisplay'));
            }
        }
        
        $annonces = $this->get('ba_buzz_annonce.annonce_manager')->getLastAnnonce($index);
        $nbResult = $this->get('ba_buzz_annonce.annonce_manager')->getNbAnnonce();
        
        return $this->render('BaBuzzAnnonceBundle:Home:index.html.twig', array('form' => $form->createView(), 'cats' => $lCategorie,
            'annonces' => $annonces, 'lIndex' => $this->getListOfIndex($nbResult / 10, $index)));
    }
    
    public function getListOfIndex($nbAnnonce, $index) {
        $res = array();
        if ($nbAnnonce <= 10) {
            for ($i = 0; $i < $nbAnnonce; ++$i) {
                $res[] = $i;
            }
        } elseif (($index - 4) >= ($nbAnnonce - 8)) {
            $res[] = 0;
            for ($i = $nbAnnonce - 8; $i <= $nbAnnonce; ++$i) {
                $res[] = $i;
            }
        } else {
            for ($i = $index - 5; $i <= $index + 5; ++$i) {
                $res[] = $i;
            }
        }
        return $res;
    }

}
