<?php

namespace Ba\BuzzAnnonceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ba\BuzzAnnonceBundle\Entity\Annonce;
use Ba\BuzzAnnonceBundle\Entity\Categorie;
use Ba\BuzzAnnonceBundle\Entity\Evaluation;
use Ba\BuzzAnnonceBundle\Form\Type\AnnonceType;
use Ba\BuzzAnnonceBundle\Form\Type\CategorieType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AnnonceController extends Controller {

    /**
     * @Route("/")
     * @Template()
     */
    public function listAction() {
        return array();
    }

    /**
     * @Route("/findByCat", name="annonce_view_cat")
     * @Template()
     */
    public function mylistAction() {
        $request = $this->get('request');

        if ('POST' == $request->getMethod()) {
            $cat = new Categorie();
            $form = $this->get('form.factory')->create(new CategorieType(), $cat); // On bind l'objet Compte à notre formulaire CompteType
            $form->bindRequest($request);
            $index = 0;
            if ($request->request->get('indexToDisplay')) {
                $index = intval($request->request->get('indexToDisplay'));
            }
            if ($request->request->get('catToDisplay')) {
                $cat->setCatLabel($request->request->get('catToDisplay'));
            }

            //isset($request->request->get('indexToDisplay')) ? intval($request->request->get('indexToDisplay')) : 0;

            $annManager = $this->get('ba_buzz_annonce.annonce_manager');
            $lAnnonce = $annManager->getByCategorie($cat->getCatLabel(), $index, $request->query->get('type'), $request->query->get('order'));
            $nbResult = $annManager->getNbAnnonceForCat($cat->getCatLabel());

            return $this->render('BaBuzzAnnonceBundle:Annonce:mylist.html.twig', array('form' => $form->createView(),
                        'annonces' => $lAnnonce, 'nbResult' => $nbResult, 'lIndex' => $this->getListOfIndex($nbResult / 10, $index),
                        'catLabel' => $cat->getCatLabel()));
        }
    }
    
    /**
     * @Route("/myAnnonce", name="annonce_view_mine")
     * @Template()
     */
    public function myannonceAction() {
        $request = $this->get('request');
        $form = $this->get('form.factory')->create(new CategorieType(), new Categorie());
        
        // Get current user
        $current_user = $this->get('security.context')->getToken()->getUser();
        if (is_object($current_user)) {
            $index = 0;
            if ('POST' == $request->getMethod()) {
                if ($request->request->get('indexToDisplay')) {
                    $index = intval($request->request->get('indexToDisplay'));
                }
            }
            $annManager = $this->get('ba_buzz_annonce.annonce_manager');
            $lAnnonce = $annManager->getUserAnnonce($current_user->getCptId(), $index, $request->query->get('type'), $request->query->get('order'));
            $nbResult = $annManager->getNbAnnonceForUser($current_user->getCptId());
            
            return $this->render('BaBuzzAnnonceBundle:Annonce:myannonce.html.twig', array('form' => $form->createView(),
                    'annonces' => $lAnnonce, 'nbResult' => $nbResult, 'lIndex' => $this->getListOfIndex($nbResult / 10, $index)));
        }
    }

    public function getListOfIndex($nbAnnonce, $index) {
        $res = array();
        if ($nbAnnonce <= 10) {
            for ($i = 0; $i < $nbAnnonce; ++$i) {
                $res[] = $i;
            }
        } elseif (($index - 4) >= ($nbAnnonce - 8)) {
            $res[] = 0;
            for ($i = $nbAnnonce - 8; $i <= $nbAnnonce; ++$i) {
                $res[] = $i;
            }
        } else {
            for ($i = $index - 5; $i <= $index + 5; ++$i) {
                $res[] = $i;
            }
        }
        return $res;
    }

    /**
     * @Route("/new", name="annonce_new"))
     * @Template()
     */
    public function newAction() {
        $request = $this->get('request'); // On récupère l'objet request via le service container
        $annonce = new Annonce(); // On créé notre objet Annonce vierge

        $form = $this->get('form.factory')->create(new AnnonceType(), $annonce); // On bind l'objet Annonce à notre formulaire AnnonceType

        if ('POST' == $request->getMethod()) { // Si on a posté le formulaire
            $form->bindRequest($request); // On bind les données du form
            if ((($_FILES["photo"]["type"] == "image/gif")
                    || ($_FILES["photo"]["type"] == "image/jpeg"))) {
                move_uploaded_file($_FILES["photo"]["tmp_name"], __DIR__ . '/../../../../web/bundles/babuzzannonce/images/' . md5($_FILES["photo"]["name"]));
            }
            $annonce->setAnnPhoto('bundles/babuzzannonce/images' . md5($_FILES["photo"]["name"]));
            $annonce->setAnnValid(1);
            $annonce->setAnnActif(1);
            $annonce->setAnnPubDate(new \DateTime('2011-01-01'));
            $annonce->setCptId($this->get('security.context')->getToken()->getUser());
            $this->get('ba_buzz_annonce.annonce_manager')->save($annonce); // On utilise notre Manager pour gérer la sauvegarde de l'objet
// On envoi une 'flash' pour indiquer à l'utilisateur que le bureau est ajouté
            $this->get('session')->setFlash('notice', $this->get('translator')->trans('Annonce added')
            );
            
            return new RedirectResponse($this->generateUrl('home'));
// On redirige vers la page de modification du bureau
//            $form = $this->get('form.factory')->create(new CategorieType(), new Categorie());
//            $lCategorie = $this->get('ba_buzz_annonce.categorie_manager')->findAll();
//            return $this->render('BaBuzzAnnonceBundle:Home:index.html.twig', array('form' => $form->createView(), 'cats' => $lCategorie));
//            
        }


        return array('form' => $form->createView(), 'annonce' => $annonce); // On passe à Twig l'objet form et notre objet annonce
    }

    /**
     * @Route("/edit/{id}", name="annonce_edit"))
     * @Template()
     */
    public function editAction($id) {
        $user = $this->get('security.context')->getToken()->getUser();
        if (!$user->getCptAdmin())
            return new RedirectResponse($this->generateUrl('home'));
        $request = $this->get('request');
        if ('POST' == $request->getMethod()) { // Si on a posté le formulaire
            $ann = $this->get('ba_buzz_annonce.annonce_manager')->getById($id);
            $annonce = new Annonce(); // On créé notre objet Annonce vierge
            $form = $this->get('form.factory')->create(new AnnonceType(), $annonce); // On bind l'objet Annonce à notre formulaire AnnonceType
            $form->bindRequest($request);
            if ((($_FILES["photo"]["type"] == "image/gif")
                    || ($_FILES["photo"]["type"] == "image/jpeg"))) {
                move_uploaded_file($_FILES["photo"]["tmp_name"], __DIR__ . '/../../../../web/bundles/babuzzannonce/images/' . md5($_FILES["photo"]["name"]));
                $annonce->setAnnPhoto('bundles/babuzzannonce/images/' . md5($_FILES["photo"]["name"]));
            } else {
                $annonce->setAnnPhoto($ann->getAnnPhoto());
            }
            $annonce->setAnnId($ann->getAnnId());
            $annonce->setCptId($ann->getCptId());
            $annonce->setComCptId($ann->getComCptId());
            $annonce->setAnnActif($ann->getAnnActif());
            $annonce->setAnnValid($ann->getAnnValid());
            $annonce->setAnnPubDate($ann->getAnnPubDate());
            $this->get('ba_buzz_annonce.annonce_manager')->update($annonce);
            $this->get('session')->setFlash('notice', $this->get('translator')->trans('Annonce mise à jour'));
            
            return new RedirectResponse($this->generateUrl('home'));
//            $form = $this->get('form.factory')->create(new CategorieType(), new Categorie());
//            $lCategorie = $this->get('ba_buzz_annonce.categorie_manager')->findAll();
//            return $this->render('BaBuzzAnnonceBundle:Home:index.html.twig', array('form' => $form->createView(), 'cats' => $lCategorie));
        } else {
            $ann = $this->get('ba_buzz_annonce.annonce_manager')->getById($id);
            $form = $this->get('form.factory')->create(new AnnonceType(), $ann);
            return $this->render('BaBuzzAnnonceBundle:Annonce:new.html.twig', array('form' => $form->createView(), 'annonce' => $ann));
        }
    }

    /**
     * @Route("/delete/{id}", name="annonce_delete"))
     * @Template()
     */
    public function deleteAction($id) {
        $user = $this->get('security.context')->getToken()->getUser();
        if (!$user->getCptAdmin())
            return new RedirectResponse($this->generateUrl('home'));
        $request = $this->get('request');
        $ann = $this->get('ba_buzz_annonce.annonce_manager')->getById($id);
        $form = $this->get('form.factory')->create(new AnnonceType(), $ann); // On bind l'objet Annonce à notre formulaire AnnonceType
        $form->bindRequest($request);

        $ann->setAnnActif(false);

        $this->get('ba_buzz_annonce.annonce_manager')->update($ann);
        $this->get('session')->setFlash('notice', $this->get('translator')->trans('Annonce mise à jour'));
        
        return new RedirectResponse($this->generateUrl('home'));
//        $form = $this->get('form.factory')->create(new CategorieType(), new Categorie());
//        return $this->render('BaBuzzAnnonceBundle:Home:index.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/show/{id}", name="annonce_show"))
     * @Template()
     */
    public function showAction($id) {
        // Init variable
        $ann = $this->get('ba_buzz_annonce.annonce_manager')->getById($id);
        $user_id = intval($ann->getCptId()->getCptId());
        
        // Get Current user
        $current_user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($current_user)) {
            $user_connected = false;
            $already_eval = null;
            $user_note = null;
        }
        else {
            $user_connected = true;
            $current_user_id = $current_user->getCptId();
            $already_eval = $this->get('ba_buzz_annonce.evaluation_manager')->alreadyEval($user_id, $current_user_id);

            // Get query string
            parse_str($_SERVER['QUERY_STRING'], $query);
            $basic_query = Array('action' => null, 'user_id' => null, 'com_id' => null, 'eval' => null);
            $query = array_merge($basic_query, $query);

            // Check if user is already evaluated
            if ($already_eval) {
                $current_evaluation = $this->get('ba_buzz_annonce.evaluation_manager')->getByIds($user_id, $current_user_id);
                $user_note = $current_evaluation->getEvalNote();
                if (isset($query['action']) && isset($query['user_id'])) {
                    $action = strip_tags($query['action']);
                    $q_user_id = intval(strip_tags($query['user_id']));
                    if ($action == "del" && $user_id == $q_user_id) {
                        $this->get('ba_buzz_annonce.evaluation_manager')->delete($current_evaluation);
                        $already_eval = false;
                    }
                }
            }
            else {
                $user_note = null;
                // Check query string
                if (isset($query['action']) && isset($query['eval']) && isset($query['user_id'])) {
                    // Clean query string
                    $action = strip_tags($query['action']);
                    $eval = intval(strip_tags($query['eval']));
                    $q_user_id = intval(strip_tags($query['user_id']));
                    if ($action == 'note' && $eval < 5 && $eval >= 0 && $q_user_id == $user_id) {
                        // Create the evaluation
                        $evaluation = new Evaluation();
                        $evaluation->setCptId($this->get('ba_buzz_annonce.compte_manager')->getById($q_user_id));
                        $evaluation->setComCptId($this->get('ba_buzz_annonce.compte_manager')->getById($current_user_id)); 
                        $evaluation->setEvalComm("---");
                        $evaluation->setEvalNote($eval);
                        $evaluation->setEvalActif(true);
                        // Save the evaluation
                        $this->get('ba_buzz_annonce.evaluation_manager')->save($evaluation);
                        $already_eval = true;
                        $user_note = $eval;
                    }
                }
            }
        }

        // Get evaluation values
        $evaluations = $this->get('ba_buzz_annonce.evaluation_manager')->getEvalByUserId($user_id);
        $eval_count = number_format(count($evaluations), 0, ',', ' ');
        $eval_average = $this->get('ba_buzz_annonce.evaluation_manager')->getEvalAverage($evaluations);
        
        // Get last url
        if (isset($_SERVER['HTTP_REFERER'])) {
            $last_url = strip_tags($_SERVER['HTTP_REFERER']);
            if (empty($last_url) || ((stristr($last_url, 'findByCat') === FALSE) && (stristr($last_url, 'myAnnonce') === FALSE)))
                $last_url = false;
        }
        else
            $last_url = false;
        
        return $this->render('BaBuzzAnnonceBundle:Annonce:show.html.twig', array('ann' => $ann, 'eval_count' => $eval_count,
            'eval_average' => $eval_average, 'already_eval' => $already_eval, 'user_note' => $user_note,
            'user_connected' => $user_connected, 'last_url' => $last_url));
    }

}
