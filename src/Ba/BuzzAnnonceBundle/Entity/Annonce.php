<?php

namespace Ba\BuzzAnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;

/**
 * Ba\BuzzAnnonceBundle\Entity\Annonce
 * @ORM\Entity(repositoryClass="Ba\BuzzAnnonceBundle\Entity\AnnonceRepository")
 * @ORM\Table(name="ANNONCE")
 */
class Annonce
{
    /**
     * @var bigint $cptId
     * @ORM\ManyToOne(targetEntity="Compte")
     * @ORM\JoinColumn(name="CPT_ID", referencedColumnName="CPT_ID")
     */
    private $cptId;

    /**
     * @var bigint $catId
     * @ORM\ManyToOne(targetEntity="Categorie")
     * @ORM\JoinColumn(name="CAT_ID", referencedColumnName="CAT_ID")
     */
    private $catId;

    /**
     * @var bigint $comCptId
     * @ORM\ManyToOne(targetEntity="Compte")
     * @ORM\JoinColumn(name="COM_CPT_ID", referencedColumnName="CPT_ID")
     * lien sur le compte à qui l'annonce a été faite
     */
    private $comCptId;

    /**
     * @var string $annPhoto
     * @ORM\Column(name="ANN_PHOTO", type="string", length=256)
     */
    private $annPhoto;

    /**
     * @var string $annDesc
     * @ORM\Column(name="ANN_DESC", type="string", length=2096)
     */
    private $annDesc;

    /**
     * @var string $annTitre
     * @ORM\Column(name="ANN_TITRE", type="string", length=2096)
     */
    private $annTitre;

    /**
     * @var integer $annValid
     * @ORM\Column(name="ANN_VALID", type="integer")
     */
    private $annValid;

    /**
     * @var boolean $annActif
     * @ORM\Column(name="ANN_ACTIF", type="boolean")
     */
    private $annActif;

    /**
     * @var date $annPubDate
     * @ORM\Column(name="ANN_PUB_DATE", type="date")
     */
    private $annPubDate;

    /**
     * @var bigint $annId
     * @ORM\Id
     * @ORM\Column(name="ANN_ID", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $annId;

    /**
     * @var date $annPubDate
     * @ORM\Column(name="ANN_PRIX", type="integer")
     */
    private $annPrix;

    /**
     * Set cptId
     *
     * @param bigint $cptId
     */
    public function setCptId($cptId)
    {
        $this->cptId = $cptId;
    }

    /**
     * Get cptId
     *
     * @return bigint 
     */
    public function getCptId()
    {
        return $this->cptId;
    }

    /**
     * Set catId
     *
     * @param bigint $catId
     */
    public function setCatId($catId)
    {
        $this->catId = $catId;
    }

    /**
     * Get catId
     *
     * @return bigint 
     */
    public function getCatId()
    {
        return $this->catId;
    }

    /**
     * Set comCptId
     *
     * @param bigint $comCptId
     */
    public function setComCptId($comCptId)
    {
        $this->comCptId = $comCptId;
    }

    /**
     * Get comCptId
     *
     * @return bigint 
     */
    public function getComCptId()
    {
        return $this->comCptId;
    }

    /**
     * Set annPhoto
     *
     * @param string $annPhoto
     */
    public function setAnnPhoto($annPhoto)
    {
        $this->annPhoto = $annPhoto;
    }

    /**
     * Get annPhoto
     *
     * @return string 
     */
    public function getAnnPhoto()
    {
        return $this->annPhoto;
    }

    /**
     * Set annDesc
     *
     * @param string $annDesc
     */
    public function setAnnDesc($annDesc)
    {
        $this->annDesc = $annDesc;
    }

    /**
     * Get annDesc
     *
     * @return string 
     */
    public function getAnnDesc()
    {
        return $this->annDesc;
    }

    /**
     * Set annTitre
     *
     * @param string $annTitre
     */
    public function setAnnTitre($annTitre)
    {
        $this->annTitre = $annTitre;
    }

    /**
     * Get annTitre
     *
     * @return string 
     */
    public function getAnnTitre()
    {
        return $this->annTitre;
    }

    /**
     * Set annValid
     *
     * @param integer $annValid
     */
    public function setAnnValid($annValid)
    {
        $this->annValid = $annValid;
    }

    /**
     * Get annValid
     *
     * @return integer 
     */
    public function getAnnValid()
    {
        return $this->annValid;
    }

    /**
     * Set annActif
     *
     * @param boolean $annActif
     */
    public function setAnnActif($annActif)
    {
        $this->annActif = $annActif;
    }

    /**
     * Get annActif
     *
     * @return boolean 
     */
    public function getAnnActif()
    {
        return $this->annActif;
    }

    /**
     * Set annPubDate
     *
     * @param date $annPubDate
     */
    public function setAnnPubDate($annPubDate)
    {
        $this->annPubDate = $annPubDate;
    }

    /**
     * Get annPubDate
     *
     * @return date 
     */
    public function getAnnPubDate()
    {
        return $this->annPubDate;
    }

    
    /**
     * Set annPubDate
     *
     * @param date $annPubDate
     */
    public function setAnnId($annId)
    {
        $this->annId = $annId;
    }
    
    /**
     * Get annId
     *
     * @return bigint 
     */
    public function getAnnId()
    {
        return $this->annId;
    }

    /**
     * Set annPrix
     *
     * @param integer $annPrix
     */
    public function setAnnPrix($annPrix)
    {
        $this->annPrix = $annPrix;
    }

    /**
     * Get annPrix
     *
     * @return integer 
     */
    public function getAnnPrix()
    {
        return $this->annPrix;
    }
}

class AnnonceRepository extends EntityRepository
{
    
}