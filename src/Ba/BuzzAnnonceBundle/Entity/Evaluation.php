<?php

namespace Ba\BuzzAnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;

/**
 * Ba\BuzzAnnonceBundle\Entity\Evaluation
 * @ORM\Entity(repositoryClass="Ba\BuzzAnnonceBundle\Entity\EvaluationRepository")
 * @ORM\Table(name="EVALUATION")
 */
class Evaluation
{
    /**
     * @var bigint $cptId
     * @ORM\ManyToOne(targetEntity="Compte")
     * @ORM\JoinColumn(name="CPT_ID", referencedColumnName="CPT_ID")
     */
    private $cptId;

    /**
     * @var bigint $comCptId
     * @ORM\ManyToOne(targetEntity="Compte")
     * @ORM\JoinColumn(name="COM_CPT_ID", referencedColumnName="CPT_ID")
     */
    private $comCptId;

    /**
     * @var string $evalComm
     * @ORM\Column(name="EVAL_COMM", type="string", length=1024)
     */
    private $evalComm;

    /**
     * @var smallint $evalNote
     * @ORM\Column(name="EVAL_NOTE", type="integer")
     */
    private $evalNote;

    /**
     * @var boolean $evalActif
     * @ORM\Column(name="EVAL_ACTIF", type="boolean")
     */
    private $evalActif;

    /**
     * @var bigint $evalId
     * @ORM\Id
     * @ORM\Column(name="EVAL_ID", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $evalId;


    /**
     * Set cptId
     *
     * @param bigint $cptId
     */
    public function setCptId($cptId)
    {
        $this->cptId = $cptId;
    }

    /**
     * Get cptId
     *
     * @return bigint 
     */
    public function getCptId()
    {
        return $this->cptId;
    }

    /**
     * Set comCptId
     *
     * @param bigint $comCptId
     */
    public function setComCptId($comCptId)
    {
        $this->comCptId = $comCptId;
    }

    /**
     * Get comCptId
     *
     * @return bigint 
     */
    public function getComCptId()
    {
        return $this->comCptId;
    }

    /**
     * Set evalComm
     *
     * @param string $evalComm
     */
    public function setEvalComm($evalComm)
    {
        $this->evalComm = $evalComm;
    }

    /**
     * Get evalComm
     *
     * @return string 
     */
    public function getEvalComm()
    {
        return $this->evalComm;
    }

    /**
     * Set evalNote
     *
     * @param smallint $evalNote
     */
    public function setEvalNote($evalNote)
    {
        $this->evalNote = $evalNote;
    }

    /**
     * Get evalNote
     *
     * @return smallint 
     */
    public function getEvalNote()
    {
        return $this->evalNote;
    }

    /**
     * Set evalActif
     *
     * @param boolean $evalActif
     */
    public function setEvalActif($evalActif)
    {
        $this->evalActif = $evalActif;
    }

    /**
     * Get evalActif
     *
     * @return boolean 
     */
    public function getEvalActif()
    {
        return $this->evalActif;
    }

    /**
     * Get evalId
     *
     * @return bigint 
     */
    public function getEvalId()
    {
        return $this->evalId;
    }
}

class EvaluationRepository extends EntityRepository
{
    
}