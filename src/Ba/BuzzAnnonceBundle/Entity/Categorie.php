<?php

namespace Ba\BuzzAnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;

/**
 * Ba\BuzzAnnonceBundle\Entity\Categorie
 * @ORM\Entity(repositoryClass="Ba\BuzzAnnonceBundle\Entity\CategorieRepository")
 * @ORM\Table(name="CATEGORIE")
 */
class Categorie
{
    /**
     * @var string $catLabel
     * @ORM\Column(name="CAT_LABEL", type="string", length=256)
     */
    private $catLabel;
    /**
     * @var bigint $catId
     * @ORM\Id
     * @ORM\Column(name="CAT_ID", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $catId;


    /**
     * Set catLabel
     *
     * @param string $catLabel
     */
    public function setCatLabel($catLabel)
    {
        $this->catLabel = $catLabel;
    }

    /**
     * Get catLabel
     *
     * @return string 
     */
    public function getCatLabel()
    {
        return $this->catLabel;
    }

    /**
     * Get catId
     *
     * @return bigint 
     */
    public function getCatId()
    {
        return $this->catId;
    }
    
    public function __toString()
    {
        return $this->getCatLabel();
    }
}

class CategorieRepository extends EntityRepository
{
    
}