<?php

namespace Ba\BuzzAnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Ba\BuzzAnnonceBundle\Entity\Compte
 * @ORM\Entity(repositoryClass="Ba\BuzzAnnonceBundle\Entity\CompteRepository")
 * @ORM\Table(name="COMPTE")
 */
class Compte implements UserInterface
{
    /**
     * @var string $cptPrenom
     * @ORM\Column(name="CPT_PRENOM", type="string", length=128)
     */
    private $cptPrenom;

    /**
     * @var string $cptNom
     * @ORM\Column(name="CPT_NOM", type="string", length=128)
     */
    private $cptNom;

    /**
     * @var string $cptCodePostal
     * @ORM\Column(name="CPT_CODE_POSTAL", type="string", length=5)
     */
    private $cptCodePostal;

    /**
     * @var bigint $catId
     * @ORM\Column(name="DEP_ID", type="string", length=128)
     */
    private $cptDepartement;

    /**
     * @var string $cptTelephone
     * @ORM\Column(name="CPT_TELEPHONE", type="string", length=10)
     */
    private $cptTelephone;

    /**
     * @var string $cptEmail
     * @ORM\Column(name="CPT_EMAIL", type="string", length=128)
     */
    private $cptEmail;

    /**
     * @var string $cptMdp
     * @ORM\Column(name="CPT_MDP", type="string", length=128)
     */
    private $cptMdp;

    /**
     * @var boolean $cptAdmin
     * @ORM\Column(name="CPT_ADMIN", type="boolean")
     */
    private $cptAdmin;

    /**
     * @var boolean $cptActif
     * @ORM\Column(name="CPT_ACTIF", type="boolean")
     */
    private $cptActif;

    /**
     * @var bigint $cptId
     * @ORM\Id
     * @ORM\Column(name="CPT_ID", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $cptId;
    

    /**
     * Set cptPrenom
     *
     * @param string $cptPrenom
     */
    public function setCptPrenom($cptPrenom)
    {
        $this->cptPrenom = $cptPrenom;
    }

    /**
     * Get cptPrenom
     *
     * @return string 
     */
    public function getCptPrenom()
    {
        return $this->cptPrenom;
    }

    /**
     * Set cptNom
     *
     * @param string $cptNom
     */
    public function setCptNom($cptNom)
    {
        $this->cptNom = $cptNom;
    }

    /**
     * Get cptNom
     *
     * @return string 
     */
    public function getCptNom()
    {
        return $this->cptNom;
    }

    /**
     * Set cptCodePostal
     *
     * @param string $cptCodePostal
     */
    public function setCptCodePostal($cptCodePostal)
    {
        $this->cptCodePostal = $cptCodePostal;
    }

    /**
     * Get cptCodePostal
     *
     * @return string 
     */
    public function getCptCodePostal()
    {
        return $this->cptCodePostal;
    }

    /**
     * Set cptDepartement
     *
     * @param string $cptDepartement
     */
    public function setCptDepartement($cptDepartement)
    {
        $this->cptDepartement = $cptDepartement;
    }

    /**
     * Get cptDepartement
     *
     * @return string 
     */
    public function getCptDepartement()
    {
        return $this->cptDepartement;
    }

    /**
     * Set cptTelephone
     *
     * @param string $cptTelephone
     */
    public function setCptTelephone($cptTelephone)
    {
        $this->cptTelephone = $cptTelephone;
    }

    /**
     * Get cptTelephone
     *
     * @return string 
     */
    public function getCptTelephone()
    {
        return $this->cptTelephone;
    }

    /**
     * Set cptEmail
     *
     * @param string $cptEmail
     */
    public function setCptEmail($cptEmail)
    {
        $this->cptEmail = $cptEmail;
    }

    /**
     * Get cptEmail
     *
     * @return string 
     */
    public function getCptEmail()
    {
        return $this->cptEmail;
    }

    /**
     * Set cptMdp
     *
     * @param string $cptMdp
     */
    public function setCptMdp($cptMdp)
    {
        $this->cptMdp = $cptMdp;
    }

    /**
     * Get cptMdp
     *
     * @return string 
     */
    public function getCptMdp()
    {
        return $this->cptMdp;
    }

    /**
     * Set cptAdmin
     *
     * @param boolean $cptAdmin
     */
    public function setCptAdmin($cptAdmin)
    {
        $this->cptAdmin = $cptAdmin;
    }

    /**
     * Get cptAdmin
     *
     * @return boolean 
     */
    public function getCptAdmin()
    {
        return $this->cptAdmin;
    }

    /**
     * Set cptActif
     *
     * @param boolean $cptActif
     */
    public function setCptActif($cptActif)
    {
        $this->cptActif = $cptActif;
    }

    /**
     * Get cptActif
     *
     * @return boolean 
     */
    public function getCptActif()
    {
        return $this->cptActif;
    }

    /**
     * Get cptId
     *
     * @return bigint 
     */
    public function getCptId()
    {
        return $this->cptId;
    }
    
    
    /*********  USER INTERFACE **********/
    /**
     * Constructs a new instance of User
     */
    public function __construct()
    {

    }    
    
    /**
     * Gets the username.
     *
     * @return string The username.
     */
    public function getUsername()
    {
        return $this->cptEmail;
    }
 
    /**
     * Sets the username.
     *
     * @param string $value The username.
     */
    public function setUsername($value)
    {
        $this->cptEmail = $value;
    }
 
    /**
     * Gets the user password.
     *
     * @return string The password.
     */
    public function getPassword()
    {
        if ($this->getCptActif())
            return $this->cptMdp;
        else
            return "toto";
        
    }
 
    /**
     * Sets the user password.
     *
     * @param string $value The password.
     */
    public function setPassword($value)
    {
        $this->cptMdp = $value;
    }
 
    /**
     * Gets the user salt.
     *
     * @return string The salt.
     */
    public function getSalt()
    {
        return "";
    }
 
    /**
     * Sets the user salt.
     *
     * @param string $value The salt.
     */
    public function setSalt($value)
    {

    }
 
    /**
     * Gets the user roles.
     *
     * @return ArrayCollection A Doctrine ArrayCollection
     */
    public function getUserRoles()
    {
        return array();
    }
 
    /**
     * Erases the user credentials.
     */
    public function eraseCredentials()
    {
 
    }
 
    /**
     * Gets an array of roles.
     * 
     * @return array An array of Role objects
     */
    public function getRoles()
    {
        return array('ROLE_USER');
    }
 
    /**
     * Compares this user to another to determine if they are the same.
     * 
     * @param UserInterface $user The user
     * @return boolean True if equal, false othwerwise.
     */
    public function equals(UserInterface $user)
    {
        return md5($this->getUsername()) == md5($user->getUsername());
    }
}

class CompteRepository extends EntityRepository
{
    
}
