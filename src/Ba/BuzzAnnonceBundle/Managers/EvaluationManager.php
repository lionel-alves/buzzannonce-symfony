<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ba\BuzzAnnonceBundle\Managers;

use Doctrine\ORM\EntityManager;
use Ba\BuzzAnnonceBundle\Managers\BaseManager;

/**
 * Description of EvaluationManager
 *
 * @author benoit
 */
class EvaluationManager extends BaseManager {

    public function __construct($em) {
        $this->em = $em;
        $this->entityName = "BaBuzzAnnonceBundle:Evaluation";
        $this->repository = $this->em->getRepository($this->entityName);
        $this->prefix = "eval";
    }

    public function update($objectToUpdate) {
        $query = $this->em->createQueryBuilder();
        $query = $query->update("Evaluation eval")
                ->set("eval.evalActif", "?", $objectToUpdate->getEvalActif())
                ->where("eval.evalId = ?", $objectToUpdate->getEvalId());
    }
    
    public function getEvalByUserId($user_id) {
        $query = $this->em->createQueryBuilder();
        $query = $query->select("eval")
                ->from('BaBuzzAnnonceBundle:Evaluation', 'eval')
                ->where('eval.cptId = :id')
                ->setParameter('id', strval($user_id));
        $res = $query->getQuery()->execute();
        return $res;
    }
    
    public function getEvalAverage($evaluations) {
        $count = count($evaluations);
        if ($count == 0)
            return " --";
        $total = 0;
        foreach ($evaluations as $eval) {
            $total += $eval->getEvalNote();
        }
        return number_format($total / $count, 2, ',', ' ');
    }
	
	public function alreadyEval($cpt_id, $com_cpt_id) {
        $query = $this->em->createQueryBuilder();
        $query = $query->from('BaBuzzAnnonceBundle:Evaluation', 'eval')
                ->select("count(eval.evalId)")
                ->where('eval.cptId = :id_cpt')
                ->andWhere('eval.comCptId = :id_com_cpt')
                ->setParameter('id_cpt', strval($cpt_id))
                ->setParameter('id_com_cpt', strval($com_cpt_id));
        
        $res = $query->getQuery()->getResult();

        $resultat = null;
        foreach ($res as $test) {
            foreach ($test as $plop) {
                $resultat = $plop;
            }
        }
        return ($resultat > 0);
    }
    
    public function getByIds($cpt_id, $com_cpt_id) {
        $query = $this->em->createQueryBuilder();
        $query = $query->select("eval")
                ->from('BaBuzzAnnonceBundle:Evaluation', 'eval')
                ->where('eval.cptId = :id_cpt')
                ->andWhere('eval.comCptId = :id_com_cpt')
                ->setParameter('id_cpt', strval($cpt_id))
                ->setParameter('id_com_cpt', strval($com_cpt_id));
        $res = $query->getQuery()->getResult();

        $resultat = null;
        foreach ($res as $test) {
            $resultat = $test;
            break;
        }
        return $resultat;
    }
}
?>
