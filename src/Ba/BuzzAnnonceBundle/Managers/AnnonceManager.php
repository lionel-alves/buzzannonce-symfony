<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ba\BuzzAnnonceBundle\Managers;

use Doctrine\ORM\EntityManager;
use Ba\BuzzAnnonceBundle\Entity\Annonce;
use Ba\BuzzAnnonceBundle\Entity\Compte;
use Ba\BuzzAnnonceBundle\Managers\BaseManager;

/**
 * Description of AnnonceManager
 *
 * @author benoit
 */
class AnnonceManager extends BaseManager {

    //put your code here

    public function __construct($em) {
        $this->em = $em;
        $this->entityName = "BaBuzzAnnonceBundle:Annonce";
        $this->repository = $this->em->getRepository($this->entityName);
        $this->prefix = "ann";
    }

    public function getLastAnnonce($index = 0, $limit = 10) {
        $query = $this->em->createQueryBuilder();
        $query = $query->select('ann')
                ->from('BaBuzzAnnonceBundle:Annonce', 'ann')
                ->where('ann.annActif = :actif')
                ->setParameter('actif', true)
                ->orderBy("ann.annPubDate", "desc");

        $query = $query->setFirstResult($index * $limit)
                ->setMaxResults($limit);
        $res = $query->getQuery()->execute();

        return $res;
    }
    
    public function getUserAnnonce($user_id, $index, $type, $order) {
        $query = $this->em->createQueryBuilder();
        $query = $query->select('ann')
                ->from('BaBuzzAnnonceBundle:Annonce', 'ann')
                ->where('ann.cptId = :user')
                ->andWhere('ann.annActif = :actif')
                ->setParameter('user', $user_id)
                ->setParameter('actif', true);
        if (null != $type && null != $order) {
            if (strval($type) == "date") {
                $query = $query->orderBy("ann.annPubDate", strval($order));
            } else {
                $query = $query->orderBy("ann.annPrix", strval($order));
            }
        }

        $query = $query->setFirstResult($index * 10)
                ->setMaxResults(10);
        $res = $query->getQuery()->execute();

        return $res;
    }
    
    public function getByCategorie($cat, $index, $type, $order) {
        $query = $this->em->createQueryBuilder();
        $query = $query->select('ann')
                ->from('BaBuzzAnnonceBundle:Annonce', 'ann')
                ->join('ann.catId', 'cat')
                ->where('cat.catLabel = :cat')
                ->andWhere('ann.annActif = :actif')
                ->setParameter('cat', $cat)
                ->setParameter('actif', true);
        if (null != $type && null != $order) {
            if (strval($type) == "date") {
                $query = $query->orderBy("ann.annPubDate", strval($order));
            } else {
                $query = $query->orderBy("ann.annPrix", strval($order));
            }
        }

        $query = $query->setFirstResult($index * 10)
                ->setMaxResults(10);
        $res = $query->getQuery()->execute();

        return $res;
    }

    /**
     * Prototype de foncion de recherches par critères (cf specs) pour les annonces
     */
    public function getBySearchCriteria($departement, $userName, $prixMin, $prixMax, $minUserMark, $categorie) {
        $query = $this->em->createQueryBuilder();
        $query = $query->select('ann', 'cpt')
                ->from('BaBuzzAnnonceBundle:Annonce', 'ann')
                ->join('ann.cptId', 'cpt');
//                ->innerJoin('ann.cat', 'cat', Join::WITH, 'cat.catId = ?2')
//                ->where($query->expr()->like('cpt.name', '?1'))
//                ->andWhere($query->expr()-_>eq('cat.catId', '?2'))
//                ->setParameters(array(1 => $userName, 2 => $categorie));
//        if (null != $prixMin) {
//            $query = $query->field('AnnPrix')->gte($prixMin);
//        }
//        if (null != $prixMax) {
//            $query = $query->field('AnnPrix')->lte($prixMax);
//        }
//        if (null != $departement) {
//            $query = $query->field('cptId.cptDpt')->equals($departement);
//        }
//        if (null != $userName) {
//            $query = $query->field('compte.cptNom')->equals($userName);            
//        }
//        if (null != $categorie) {
//            $query = $query->field('cate.catId')->equals($categorie);
//        }

        return $query->getQuery()->execute();
    }

    public function getNbAnnonceForUser($userId) {
        /*$query = $this->em->createQueryBuilder();
        $query = $query->select('ann')
                ->from('BaBuzzAnnonceBundle:Annonce', 'ann')
                ->join('ann.comCptId', 'cpt')
                ->where($query->expr()->eq('cpt.cptId', '?1'));

        $query = $query->setParameters(array(1 => $userId));

        return $query->getQuery()->execute();*/
        $query = $this->em->createQueryBuilder();
        $query = $query->select('count(ann.annId)')
                ->from('BaBuzzAnnonceBundle:Annonce', 'ann')
                ->where('ann.cptId = :user')
                ->andWhere('ann.annActif = :actif')
                ->setParameter('user', $userId)
                ->setParameter('actif', true);
        
        $res = $query->getQuery()->getResult();

        $resultat = null;
        foreach ($res as $test) {
            foreach ($test as $plop) {
                $resultat = $plop;
            }
        }
        return $resultat;
    }

    public function getNbAnnonce() {
        $query = $this->em->createQueryBuilder();
        $query = $query->select('count(ann.annId)')
                ->from('BaBuzzAnnonceBundle:Annonce', 'ann')
                ->where('ann.annActif = :actif')
                ->setParameter('actif', true);
        
        $res = $query->getQuery()->getResult();
        
        foreach ($res as $test) {
            foreach ($test as $plop) {
                $resultat = $plop;
            }
        }
        return $resultat;
    }

    public function getNbAnnonceForCat($cat) {
        $query = $this->em->createQueryBuilder();
        $query = $query->select('count(ann.annId)')
                ->from('BaBuzzAnnonceBundle:Annonce', 'ann')
                ->join('ann.catId', 'cat')
                ->where('cat.catLabel = :cat')
                ->andWhere('ann.annActif = :actif')
                ->setParameter('cat', $cat)
                ->setParameter('actif', true);
        
        $res = $query->getQuery()->getResult();

        $resultat = null;
        foreach ($res as $test) {
            foreach ($test as $plop) {
                $resultat = $plop;
            }
        }
        return $resultat;
    }

    public function getById($id) {
        $query = $this->em->createQueryBuilder();
        $query = $query->select("ann")
                ->from('BaBuzzAnnonceBundle:Annonce', 'ann')
                ->where('ann.annId = :id')
                ->setParameter('id', strval($id));
        $res = $query->getQuery()->getResult();

        $resultat = null;
        foreach ($res as $test) {
            $resultat = $test;
            break;
        }
        return $resultat;
    }

    public function update($objectToUpdate) {
        $query = $this->em->createQueryBuilder();
        $query = $query->update("BaBuzzAnnonceBundle:Annonce ann")
                ->set("ann.cptId", ":cptId")
                ->set("ann.catId", ":catId")
                ->set("ann.comCptId", ":comCptId")
                ->set("ann.annPhoto", ":annPhoto")
                ->set("ann.annDesc", ":annDesc")
                ->set("ann.annTitre", ":annTitre")
                ->set("ann.annValid", ":annValid")
                ->set("ann.annActif", ":annActif")
                ->set("ann.annPubDate", ":annPubDate")
                ->set("ann.annPrix", ":annPrix")
                ->where("ann.annId = :id")
                ->setParameter("cptId", $objectToUpdate->getCptId())
                ->setParameter("catId", $objectToUpdate->getCatId())
                ->setParameter("comCptId", $objectToUpdate->getComCptId())
                ->setParameter("annPhoto", $objectToUpdate->getAnnPhoto())
                ->setParameter("annDesc", $objectToUpdate->getAnnDesc())
                ->setParameter("annTitre", $objectToUpdate->getAnnTitre())
                ->setParameter("annValid", $objectToUpdate->getAnnValid())
                ->setParameter("annActif", $objectToUpdate->getAnnActif())
                ->setParameter("annPubDate", $objectToUpdate->getAnnPubDate())
                ->setParameter("annPrix", $objectToUpdate->getAnnPrix())
                ->setParameter('id', $objectToUpdate->getAnnId());

        $query->getQuery()->execute();
    }

}
