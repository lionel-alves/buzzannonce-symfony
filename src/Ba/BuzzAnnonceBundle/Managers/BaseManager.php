<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ba\BuzzAnnonceBundle\Managers;

/**
 * Description of BaseManager
 *
 * @author benoit
 */
abstract class BaseManager {

    protected $em;
    protected $entityName;
    protected $repository;
    protected $prefix = "";

    //put your code here
    public function save($entity) {
        $this->em->persist($entity);
        $this->em->flush();
    }
    
    public function delete($entity) {
        $this->em->remove($entity);
        $this->em->flush();
    }
}