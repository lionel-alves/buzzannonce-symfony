<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ba\BuzzAnnonceBundle\Managers;

use Doctrine\ORM\EntityManager;
use Ba\BuzzAnnonceBundle\Entity\Compte;
use Ba\BuzzAnnonceBundle\Managers\BaseManager;

/**
 * Description of CompteManager
 *
 * @author benoit
 */
class CompteManager extends BaseManager {

    /**
     * Cf the services configuration
     * @param type $em 
     */
    public function __construct($em) {
        $this->em = $em;
        $this->entityName = "BaBuzzAnnonceBundle:Compte";
        $this->repository = $this->em->getRepository($this->entityName);
        $this->prefix = "cpt";
    }

    public function getById($id) {
        $query = $this->em->createQueryBuilder();
        $query = $query->select("cpt")
                ->from('BaBuzzAnnonceBundle:Compte', 'cpt')
                ->where('cpt.cptId = :id')
                ->setParameter('id', $id);
        $res = $query->getQuery()->getResult();

        $resultat = null;
        foreach ($res as $test) {
            $resultat = $test;
            break;
        }
        return $resultat;
    }

    public function getCompteByEmail($email) {
        $query = $this->em->createQueryBuilder();
        $query = $query->select("cpt")
                ->from('BaBuzzAnnonceBundle:Compte', 'cpt')
                ->where('cpt.cptEmail = :email')
                ->setParameter('email', $email);

        $res = $query->getQuery()->execute();
        $resultat = null;
        foreach ($res as $test) {
            $resultat = $test;
            break;
        }
        return $resultat;
    }

    public function update($objectToUpdate) {
        $query = $this->em->createQueryBuilder();
        $query = $query->update("BaBuzzAnnonceBundle:Compte cpt")
                ->set("cpt.cptNom", ":cptNom")
                ->set("cpt.cptPrenom", ":cptPrenom")
                ->set("cpt.cptCodePostal", ":cptCodePostal")
                ->set("cpt.cptDepartement", ":cptDepartement")
                ->set("cpt.cptTelephone", ":cptTelephone")
                ->set("cpt.cptEmail", ":cptEmail")
                ->set("cpt.cptMdp", ":cptMdp")
                ->set("cpt.cptAdmin", ":cptAdmin")
                ->set("cpt.cptActif", ":cptActif")
                ->where("cpt.cptId = :id")
                ->setParameter('id', $objectToUpdate->getCptId())
                ->setParameter('cptNom', $objectToUpdate->getCptNom())
                ->setParameter('cptPrenom', $objectToUpdate->getCptPrenom())
                ->setParameter('cptCodePostal', $objectToUpdate->getCptCodePostal())
                ->setParameter('cptDepartement', $objectToUpdate->getCptDepartement())
                ->setParameter('cptTelephone', $objectToUpdate->getCptTelephone())
                ->setParameter('cptEmail', $objectToUpdate->getCptEmail())
                ->setParameter('cptMdp', $objectToUpdate->getCptMdp())
                ->setParameter('cptAdmin', $objectToUpdate->getCptAdmin())
                ->setParameter('cptActif', $objectToUpdate->getCptActif());

        $query->getQuery()->execute();
    }

    public function findById($id) {
        $query = $this->em->createQueryBuilder();
        $query = $query->select("cpt")
                ->from("BaBuzzAnnonceBundle:Compte", "cpt")
                ->where($query->expr()->eq('cpt.cptId', $id));
        $arr = $query->getQuery()->getResult();
        return $arr[0];
    }
    
    public function findAll() {
        $query = $this->em->createQueryBuilder();
        $query = $query->select("cpt")
                ->from("BaBuzzAnnonceBundle:Compte", "cpt");;
        $arr = $query->getQuery()->getResult();
        return $arr;
    }
}
