<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ba\BuzzAnnonceBundle\Managers;

use Doctrine\ORM\EntityManager;
use Ba\BuzzAnnonceBundle\Managers\BaseManager;

/**
 * Description of CategorieManager
 *
 * @author benoit
 */
class CategorieManager extends BaseManager {

    public function __construct($em) {
        $this->em = $em;
        $this->entityName = "BaBuzzAnnonceBundle:Categorie";
        $this->repository = $this->em->getRepository($this->entityName);
        $this->prefix = "cat";
    }

    public function findAll() {
        $query = $this->em->createQueryBuilder();
        $query = $query->select('cat')
                ->from('BaBuzzAnnonceBundle:Categorie', 'cat');
    
        return $query->getQuery()->execute();
    }
    
    public function getById($id) {
        $query = $this->em->createQueryBuilder();
        $query = $query->select("cat")
                ->from('BaBuzzAnnonceBundle:Categorie', 'cat')
                ->where('cat.catId = :id')
                ->setParameter('id', strval($id));
        $res = $query->getQuery()->getResult();

        $resultat = null;
        foreach ($res as $test) {
            $resultat = $test;
            break;
        }
        return $resultat;
    }

    public function deleteById($id) {
        $query = $this->em->createQueryBuilder();
        $query = $query->delete()
                ->from('BaBuzzAnnonceBundle:Categorie', 'cat')
                ->where('cat.catId = :id')
                ->setParameter('id', strval($id));

        return $query->getQuery()->execute();
    }
}

?>
