<?php

namespace Ba\BuzzAnnonceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class LoginType extends AbstractType {

    public function buildForm(FormBuilder $builder, array $options) {
        $builder
                ->add('cptEmail', 'email')
                ->add('cptMdp','password');
    }

    public function getDefaultOptions(array $options) {
        return array(
            'data_class' => 'Ba\BuzzAnnonceBundle\Entity\Compte',
        );
    }

    public function getName() {
        return 'Compte';
    }

}