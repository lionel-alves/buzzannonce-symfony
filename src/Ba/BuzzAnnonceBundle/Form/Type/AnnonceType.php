<?php

namespace Ba\BuzzAnnonceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class AnnonceType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('annPhoto')
            ->add('catId')
            ->add('annTitre')
            ->add('annDesc')
            ->add('annValid')
            ->add('annPrix');
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Ba\BuzzAnnonceBundle\Entity\Annonce',
        );
    }

    public function getName()
    {
        return 'Annonce';
    }
}