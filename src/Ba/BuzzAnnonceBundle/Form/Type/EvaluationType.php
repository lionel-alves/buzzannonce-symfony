<?php

namespace Ba\BuzzAnnonceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class EvaluationType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
       
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Ba\BuzzAnnonceBundle\Entity\Evaluation',
        );
    }

    public function getName()
    {
        return 'Evaluation';
    }
}