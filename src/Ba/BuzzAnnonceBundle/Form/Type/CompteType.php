<?php

namespace Ba\BuzzAnnonceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class CompteType extends AbstractType {

    public function buildForm(FormBuilder $builder, array $options) {
        $builder
                ->add('cptPrenom', 'text')
                ->add('cptNom', 'text')
                ->add('cptCodePostal', 'number')
                ->add('cptDepartement')
                ->add('cptTelephone', 'number')
                ->add('cptEmail', 'email')
                ->add('cptMdp', 'repeated', array(
                    'first_name' => 'Mot de passe',
                    'second_name' => 'Confirmation ',
                    'type' => 'password'
            ));
    }

    public function getDefaultOptions(array $options) {
        return array(
            'data_class' => 'Ba\BuzzAnnonceBundle\Entity\Compte',
        );
    }

    public function getName() {
        return 'Compte';
    }

}
