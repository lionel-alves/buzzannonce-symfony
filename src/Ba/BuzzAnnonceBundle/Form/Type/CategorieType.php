<?php

namespace Ba\BuzzAnnonceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class CategorieType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('catLabel');
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Ba\BuzzAnnonceBundle\Entity\Categorie',
        );
    }

    public function getName()
    {
        return 'Categorie';
    }
}
