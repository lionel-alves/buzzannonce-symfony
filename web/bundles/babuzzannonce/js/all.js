/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function setTheForm(){
    var valToSet = document.getElementById('Categorie_catLabel');
    var listOfOpt = document.getElementById('selectOfCat');

    valToSet.value = listOfOpt.options[listOfOpt.selectedIndex].value;

    document.getElementById('formToSub').submit();
}

function reloadListView() {
    document.getElementById('reloader').submit();
}

function goToIndex(index) {
    document.getElementById('indexToDisplay').value = index;
    var order = $_GET("order");
    var type = $_GET("type");
    if (null != type) {
        document.getElementById('reloader').action = document.getElementById('reloader').action
            + "?" + "type=" + type + "&order=" + order;
    }
    reloadListView();
}

function setOrderListView(type) {
    var order = $_GET("order");
    if (null != type) {
        if (null != order) {
            if (order == "asc") {
                order = "desc";
            } else {
                order = "asc";
            }
        } else {
            order = "asc";
        }
        document.getElementById('reloader').action = document.getElementById('reloader').action
            + "?" + "type=" + type + "&order=" + order;
        reloadListView();
    }
}

function $_GET(variable) {
    var query = window.location.search.substring(1); 
    var vars = query.split("&"); 
    for (var i=0;i<vars.length;i++)
    { 
        var pair = vars[i].split("="); 
        if (pair[0] == variable)
        { 
            return pair[1]; 
        } 
    }
    return null; //not found 
} 